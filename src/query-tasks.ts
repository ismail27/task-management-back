import { TaskStatus } from './tasks/task-status.enum';
import { IsEnum, IsOptional } from 'class-validator';

export class QueryTasks {
  @IsOptional()
  @IsEnum(TaskStatus)
  status: TaskStatus;

  @IsOptional()
  search: string;
}
