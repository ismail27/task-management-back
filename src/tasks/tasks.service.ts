import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task-dto';
import { UpdateTaskDto } from './dto/update-task-dto';
import { QueryTasks } from '../query-tasks';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';
import { Like } from 'typeorm';

@Injectable()
export class TasksService {
  constructor(@InjectRepository(Task) private taskRepository: TaskRepository) {}

  getAllTasks(query: QueryTasks): Promise<Task[]> {
    return this.taskRepository.find({
      where: [
        {
          title: Like(`%${query.search}%`),
          status: query.status || 'TRUE',
        },
        {
          description: Like(`%${query.search}%`),
          status: query.status || 'TRUE',
        },
      ],
    });
    /*
    return this.tasks.filter(
      (task) =>
        (query.status ? task.status === query.status : true) &&
        (task.title.includes(query.search ?? '') ||
          task.description.includes(query.search ?? '')),
    );*/
  }

  createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    const task = this.taskRepository.create(createTaskDto);
    return this.taskRepository.save(task);
    /*const task: Task = {
      id: uuidv4(),
      title: createTaskDto.title,
      description: createTaskDto.description,
      status: TaskStatus.OPEN,
      priority: createTaskDto.priority,
    };
    this.tasks.push(task);
    return task;*/
  }

  async getTaskById(id: number): Promise<Task> {
    const task = await this.taskRepository.findOne(id);
    if (!task) {
      throw new NotFoundException(`Can not find task with id: ${id}`);
    }
    return task;

    // return this.tasks.find((task) => task.id === params.id);
  }

  deleteTaskById(id: number): void {
    // this.tasks = this.tasks.filter((task) => task.id !== params.id);
  }

  updateTask(updateTaskDto: UpdateTaskDto): Promise<Task> {
    return null;
    /*const task = this.tasks.find((task) => task.id === updateTaskDto.id);
    const index = this.tasks.indexOf(task);
    this.tasks[index] = { ...task, ...updateTaskDto };
    return this.tasks[index];*/
  }
}
