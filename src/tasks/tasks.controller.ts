import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task-dto';
import { FindOneParams } from '../find-one-params';
import { UpdateTaskDto } from './dto/update-task-dto';
import { QueryTasks } from '../query-tasks';
import { Task } from './task.entity';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @Get()
  getAllTasks(@Query() queryTasks: QueryTasks): Promise<Task[]> {
    return this.tasksService.getAllTasks(queryTasks);
  }

  @Post()
  createTask(@Body() createTaskDto: CreateTaskDto): Promise<Task> {
    return this.tasksService.createTask(createTaskDto);
  }

  @Get(':id')
  getTaskById(@Param() params: FindOneParams): Promise<Task> {
    return this.tasksService.getTaskById(params.id);
  }

  @Delete(':id')
  deleteTaskById(@Param() params: FindOneParams): void {
    return this.tasksService.deleteTaskById(params.id);
  }

  @Put()
  updateTask(@Body() updateTaskDto: UpdateTaskDto): Promise<Task> {
    return this.tasksService.updateTask(updateTaskDto);
  }
}
